import os.path


def get_python_callers(workbench_path):
    files_dct = {}

    workbench_lines = open(workbench_path).read()
    transformers = workbench_lines.split("<TRANSFORMER")
    for transformer in transformers:
        if 'TYPE="PythonCaller"' in transformer:
            transformer_lines = transformer.split("\n")
            for line in transformer_lines:
                if '<XFORM_PARM PARM_NAME="XFORMER_NAME" PARM_VALUE=' in line:
                    name = line[line.find("PARM_VALUE=") + len("PARM_VALUE=")+1:-3] + ".py"
                if "PYTHONSOURCE" in line:
                    index = line.find("PARM_VALUE=")
                    converted_line = line[index + len("PARM_VALUE="):-3].replace("&lt;space&gt;", " ") \
                        .replace("&lt;lf&gt;", "\n") \
                        .replace("&lt;openparen&gt;", "(") \
                        .replace("&lt;closeparen&gt;", ")") \
                        .replace("&lt;quote&gt;", '"') \
                        .replace("&lt;apos&gt;", "'") \
                        .replace("&lt;comma&gt;", ', ') \
                        .replace("&lt;solidus&gt;", "/") \
                        .replace("&lt;openbracket&gt;", "[") \
                        .replace("&lt;closebracket&gt;", "]") \
                        .replace("&lt;at&gt;", "@")\
                        .replace("&lt;gt&gt;", ">")\
                        .replace("&lt;lt&gt;", "<")
            files_dct[name] = converted_line
    return files_dct


def extract_pcallers_to_files(python_callers_dict):
    for name, val in python_callers_dict.items():
        with open(name, "w") as new_fl:
            new_fl.write(val[1:])


def update_python_transformer_source(workbench_path, transformer_name, source_code):
    workbench_lines = open(workbench_path).read()
    transformers = workbench_lines.split("<TRANSFORMER")
    workbench_base_path, extension = os.path.splitext(workbench_path)
    newf = open(f"{workbench_base_path}_new{extension}", "w")
    for transformer in transformers:
        if f'"{transformer_name}"' not in transformer:
            newf.write("<TRANSFORMER"+transformer)
        else:
            transformer_lines = transformer.split("\n")
            new_transformer_lines = []
            for line in transformer_lines:
                if "PYTHONSOURCE" in line:
                    code_fragment = line[line.find("PARM_VALUE=") + len("PARM_VALUE=") + 1: line.rfind('"')]
                    new_line = line.replace(code_fragment, source_code)
                    new_transformer_lines.append(new_line)
                else:
                    new_transformer_lines.append(line)
            newf.write("<TRANSFORMER" + "\n".join(new_transformer_lines))
    newf.close()
    return f"{workbench_base_path}_new{extension}"


def update_python_factory(workbench_path, transformer_name, source_code):
    new_workbench = open("newwb.fmw", "w")

    with open(workbench_path) as wbfile:
        workbench_lines = wbfile.readlines()
        for line in workbench_lines:
            if "FACTORY_DEF {*} PythonFactory" in line and f' {{ {transformer_name} }}' in line:
                python_code = line[line.find("SOURCE_CODE { ") + len("SOURCE_CODE { "): line.rfind("}    OUTPUT")]
                new_line = line.replace(python_code, source_code)
                new_workbench.write(new_line)
            else:
                new_workbench.write(line)


fme_workbench_path = r"shapefile2json.fmw"
callers = get_python_callers(fme_workbench_path)
extract_pcallers_to_files(callers)
updated = update_python_transformer_source(fme_workbench_path, "PythonCallerTEST",
                                           f"from PythonCallerTEST import *".replace(" ", "&lt;space&gt;"))
update_python_factory(updated, "PythonCallerTEST", f"from PythonCallerTEST import *".replace(" ", "<space>"))
