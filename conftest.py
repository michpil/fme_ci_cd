import pytest
import get_metadata


def pytest_configure(config):
    config._metadata["Datetime"] = get_metadata.get_datetime()
    config._metadata["Designer"] = get_metadata.get_designer()
    config._metadata["Test Type"] = get_metadata.get_test_type()
    config._metadata["Tester"] = get_metadata.get_tester()
    config._metadata["Pytest"] = get_metadata.get_pytest_ver()
    config._metadata["Python Version"] = get_metadata.get_python_ver()
    config._metadata["System Version"] = get_metadata.get_system_ver()
