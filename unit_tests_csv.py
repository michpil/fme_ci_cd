import fme
import fmeobjects


logger = fmeobjects.FMELogFile()


def test_IN_CSV_Columns_Name(feature):
    # read data
    id = feature.getAttribute('ID')
    actual = feature.getAllAttributeNames()
    expected = ['ID', 'LAT', 'LON', 'LETTER']

    # transformation
    if id == 10:
        # logging
        if not all(item in actual for item in expected):
            logger.logMessageString(f"UNIT TEST | {test_IN_CSV_Columns_Name.__name__} | COLUMNS NAME | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_IN_CSV_Row_Number(feature):
    # read data
    actual = feature.getAttribute('ID')
    expected = 10

    # transformation
    if actual == 10:
        # logging
        if actual != expected:
            logger.logMessageString(f"UNIT TEST | {test_IN_CSV_Row_Number.__name__} | ROWS NUMBER | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_TRX_AttributeValueMapper_LETTER2CODE(feature):
    # read data
    source = 'LETTER'
    target = 'CODE'
    input = feature.getAttribute(source)
    actual = feature.getAttribute(target)
    id = feature.getAttribute('ID')

    # transformation
    if input == 'A':
        expected = '1'
    elif input == 'B':
        expected = '2'
    elif input == 'C':
        expected = '3'
    elif input == 'D':
        expected = '4'
    else:
        expected = 'NULL'

    # logging
    if actual != expected:
        logger.logMessageString(f"UNIT TEST | {test_TRX_AttributeValueMapper_LETTER2CODE.__name__} | COLUMN: {target} | ID: {id} | ACTUAL: {actual} | EXPECTED: {expected}", fmeobjects.FME_ERROR)


def test_TRX_PythonCaller_LETTER_CODE(feature):
    # read data
    source1 = 'LETTER'
    source2 = 'CODE'
    target = 'LETTER_CODE'
    input1 = feature.getAttribute(source1)
    input2 = feature.getAttribute(source2)
    actual = feature.getAttribute(target)
    id = feature.getAttribute('ID')

    # transformation
    expected = f'{input1}_{input2}'

    # logging
    if actual != expected:
        logger.logMessageString(f"UNIT TEST | {test_TRX_PythonCaller_LETTER_CODE.__name__} | COLUMN: {target} | ID: {id} | ACTUAL: {actual} | EXPECTED: {expected}", fmeobjects.FME_ERROR)


def test_OUT_CSV_Columns_Name(feature):
    # read data
    id = feature.getAttribute('ID')
    actual = feature.getAllAttributeNames()
    expected = ['ID', 'LAT', 'LON', 'LETTER', 'CODE', 'LETTER_CODE']

    # transformation
    if id == 10:
        # logging
        if not all(item in actual for item in expected):
            logger.logMessageString(f"UNIT TEST | {test_OUT_CSV_Columns_Name.__name__} | COLUMNS NAME | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_OUT_CSV_Row_Number(feature):
    # read data
    actual = feature.getAttribute('ID')
    expected = 10

    # transformation
    if actual == 10:
        # logging
        if actual != expected:
            logger.logMessageString(f"UNIT TEST | {test_OUT_CSV_Row_Number.__name__} | ROWS NUMBER | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)
