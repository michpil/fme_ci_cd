import platform
import pytest
import struct
from datetime import datetime
import getpass


def get_datetime():
    dt = datetime.now()
    return dt


def get_designer():
    designer = 'Test Team'
    return designer


def get_test_type():
    type = 'Automated'
    return type


def get_tester():
    tester = getpass.getuser()
    return tester


def get_pytest_ver():
    rf_ver = pytest.__version__
    return rf_ver


def get_python_ver():
    py_ver = f'(Python {platform.python_version()} on win{struct.calcsize("P") * 8})'
    return py_ver


def get_system_ver():
    system_ver = platform.platform()
    return system_ver
