import fme
import fmeobjects
import geopandas as gpd
import pandas as pd
from shapely.geometry import Point, LineString


logger = fmeobjects.FMELogFile()


def test_IN_CSV_Columns_Name(feature):
    # read data
    id = feature.getAttribute('ID')
    actual = feature.getAllAttributeNames()
    expected = ['ID', 'LAT', 'LON', 'LETTER']

    # transformation
    if id == 10:
        # logging
        if not all(item in actual for item in expected):
            logger.logMessageString(f"UNIT TEST | {test_IN_CSV_Columns_Name.__name__} | COLUMNS NAME | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_IN_CSV_Row_Number(feature):
    # read data
    actual = feature.getAttribute('ID')
    expected = 10

    # transformation
    if actual == 10:
        # logging
        if actual != expected:
            logger.logMessageString(f"UNIT TEST | {test_IN_CSV_Row_Number.__name__} | ROWS NUMBER | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_TRX_AttributeValueMapper_LETTER2CODE(feature):
    # read data
    source = 'LETTER'
    target = 'CODE'
    input = feature.getAttribute(source)
    actual = feature.getAttribute(target)
    id = feature.getAttribute('ID')

    # transformation
    if input == 'A':
        expected = '1'
    elif input == 'B':
        expected = '2'
    elif input == 'C':
        expected = '3'
    elif input == 'D':
        expected = '4'
    else:
        expected = 'NULL'

    # logging
    if actual != expected:
        logger.logMessageString(f"UNIT TEST | {test_TRX_AttributeValueMapper_LETTER2CODE.__name__} | COLUMN: {target} | ID: {id} | ACTUAL: {actual} | EXPECTED: {expected}", fmeobjects.FME_ERROR)


def test_TRX_PythonCaller_LETTER_CODE(feature):
    # read data
    source1 = 'LETTER'
    source2 = 'CODE'
    target = 'LETTER_CODE'
    input1 = feature.getAttribute(source1)
    input2 = feature.getAttribute(source2)
    actual = feature.getAttribute(target)
    id = feature.getAttribute('ID')

    # transformation
    expected = f'{input1}_{input2}'

    # logging
    if actual != expected:
        logger.logMessageString(f"UNIT TEST | {test_TRX_PythonCaller_LETTER_CODE.__name__} | COLUMN: {target} | ID: {id} | ACTUAL: {actual} | EXPECTED: {expected}", fmeobjects.FME_ERROR)


def test_OUT_SHP_P_Columns_Name(feature):
    # read data
    id = feature.getAttribute('ID')
    actual = feature.getAllAttributeNames()
    expected = ['ID', 'LAT', 'LON', 'LETTER', 'CODE', 'LETTER_CODE']

    # transformation
    if id == 10:
        # logging
        if not all(item in actual for item in expected):
            logger.logMessageString(f"UNIT TEST | {test_OUT_SHP_P_Columns_Name.__name__} | COLUMNS NAME | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_OUT_SHP_P_Row_Number(feature):
    # read data
    actual = feature.getAttribute('ID')
    expected = 10

    # transformation
    if actual == 10:
        # logging
        if actual != expected:
            logger.logMessageString(f"UNIT TEST | {test_OUT_SHP_P_Row_Number.__name__} | ROWS NUMBER | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_OUT_SHP_L_Row_Number(feature):
    # read data
    actual = feature.getAttribute('ID')
    expected = 1

    # transformation
    if actual == 1:
        # logging
        if actual != expected:
            logger.logMessageString(f"UNIT TEST | {test_OUT_SHP_L_Row_Number.__name__} | ROWS NUMBER | ACTUAL: {actual} | EXPECTED: {expected}",
                                    fmeobjects.FME_ERROR)


def test_TRX_VertexCreator_XY2POINTS(feature):
    # read data
    actual_id = feature.getAttribute('ID')
    fme_dataset = feature.getAttribute('fme_dataset')
    expected_df = pd.read_csv(fme_dataset)

    # transformation
    expected_gdf = gpd.GeoDataFrame(
        expected_df, geometry=gpd.points_from_xy(expected_df.LON, expected_df.LAT, crs="EPSG:4326")).loc[actual_id - 1]
    expected_geom = expected_gdf.geometry

    actual_points = tuple(feature.getGeometry().getXYZ())[:2]
    actual_geom = Point(actual_points)

    is_geom_equals = actual_geom.equals_exact(expected_geom, tolerance=0)

    # logging
    if not is_geom_equals:
        logger.logMessageString(
            f"UNIT TEST | {test_TRX_VertexCreator_XY2POINTS.__name__} | COLUMN: GEOMETRY | ID: {actual_id} | ACTUAL: {actual_geom} | EXPECTED: {expected_geom}",
            fmeobjects.FME_ERROR)


def test_TRX_LineBuilder_POINTS2LINE(feature):
    # read data
    actual_id = feature.getAttribute('ID')
    fme_dataset = feature.getAttribute('fme_dataset')
    expected_df = pd.read_csv(fme_dataset)

    # transformation
    expected_points = [Point(xy) for xy in zip(expected_df.LON, expected_df.LAT)]
    expected_geom = LineString(expected_points)

    actual_points = [tuple(point.getXYZ()[:2]) for point in feature.getGeometry().getPoints()]
    actual_geom = LineString(actual_points)

    is_geom_equals = actual_geom.equals_exact(expected_geom, tolerance=0)

    # logging
    if not is_geom_equals:
        logger.logMessageString(
            f"UNIT TEST | {test_TRX_LineBuilder_POINTS2LINE.__name__} | COLUMN: GEOMETRY | ID: {actual_id} | ACTUAL: {actual_geom} | EXPECTED: {expected_geom}",
            fmeobjects.FME_ERROR)
