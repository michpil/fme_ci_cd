

def log_transformer_coverage(fwm_file):
    with open(fwm_file) as file:
        text_lines = file.readlines()

    transformer_line = []
    unit_test_line = []
    for line in text_lines:
        if 'PARM_VALUE="TRX' in line:
            line = line.replace('#!     <XFORM_PARM PARM_NAME="XFORMER_NAME" PARM_VALUE="', '').replace(r'/>', '').replace(
                '\n', '')
            transformer_line.append(line)
        if 'PARM_VALUE="UNIT_TEST' in line:
            line = line.replace('#!     <XFORM_PARM PARM_NAME="XFORMER_NAME" PARM_VALUE="', '').replace(r'/>', '').replace('\n', '')
            unit_test_line.append(line)

    print('\n' + 30 * '#' + ' TRANSFORMERS COVERAGE ' + 30 * '#')
    unit_test_line_ = [unit_test.split('_')[4] for unit_test in unit_test_line]
    cov_id = []
    for transformer in transformer_line:
        transformer_ = transformer.split('_')[1].strip('"')
        if transformer_ in unit_test_line_:
            print(transformer + ' COVERED')
            cov_id.append(True)
        else:
            print(transformer + ' NOT COVERED')
            cov_id.append(False)
    if all(cov_id):
        print('\nALL TRANSFORMERS COVERED\n')
    else:
        print('\nNOT ALL TRANSFORMERS COVERED\n')
    print(83 * '#' + '\n')
