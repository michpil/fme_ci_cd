import pytest

logfile_name = 'csv2csv.log'


with open(logfile_name) as file:
    logfile_text = file.readlines()


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_IN_CSV_Columns_Name' in line] + [None])
def test_IN_CSV_Columns_Name(log_record):
    assert not log_record


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_IN_CSV_Row_Number' in line] + [None])
def test_IN_CSV_Row_Number(log_record):
    assert not log_record


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_TRX_AttributeValueMapper_LETTER2CODE' in line] + [None])
def test_TRX_AttributeValueMapper_LETTER2CODE(log_record):
    assert not log_record


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_TRX_PythonCaller_LETTER_CODE' in line] + [None])
def test_TRX_PythonCaller_LETTER_CODE(log_record):
    assert not log_record


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_OUT_CSV_Columns_Name' in line] + [None])
def test_OUT_CSV_Columns_Name(log_record):
    assert not log_record


@pytest.mark.parametrize("log_record", [line.replace('\n', '') for line in logfile_text if 'UNIT TEST | test_OUT_CSV_Row_Number' in line] + [None])
def test_OUT_CSV_Row_Number(log_record):
    assert not log_record
